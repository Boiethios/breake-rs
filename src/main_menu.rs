use ::MyWindow;
use ::conrod;
use piston_window::Window;

#[derive(Debug, Copy, Clone)]
pub enum MainMenu {
    Play,
    Restart,
    HighScores,
    Options,
    About,
    Quit,
}

impl MainMenu {
    fn values() -> &'static [MainMenu] {
        use MainMenu::*;
        
        &[Play, Restart, HighScores, Options, About, Quit]
    }

    fn label(&self) -> &'static str {
        use MainMenu::*;

        match *self {
            Play        => "Continue",
            Restart     => "Restart",
            HighScores  => "High Scores",
            Options     => "Options",
            About       => "About",
            Quit        => "Quit",
        }
    }

    fn canvas_id(&self, ids: &Ids) -> conrod::widget::Id {
        use MainMenu::*;

        match *self {
            Play        => ids.canvas_play,
            Restart     => ids.canvas_restart,
            HighScores  => ids.canvas_high,
            Options     => ids.canvas_options,
            About       => ids.canvas_about,
            Quit        => ids.canvas_quit,
        }
    }

    fn button_id(&self, ids: &Ids) -> conrod::widget::Id {
        use MainMenu::*;

        match *self {
            Play        => ids.button_play,
            Restart     => ids.button_restart,
            HighScores  => ids.button_high,
            Options     => ids.button_options,
            About       => ids.button_about,
            Quit        => ids.button_quit,
        }
    }

    fn set_button(&mut self, ui: &mut conrod::UiCell, ids: &Ids,
                  result: &mut bool, action: MainMenu) {
        use conrod::{widget, Positionable, Labelable, Sizeable, Widget};

        for _press in widget::Button::new()
            .middle_of(action.canvas_id(ids))
            .label(action.label())
            .w_h(300.0, 80.0)
            .set(action.button_id(ids), ui)
        {
            *self = action;
            *result = true;
        }
    }

    fn gui(&mut self, ui: &mut conrod::UiCell, ids: &Ids) -> bool {
        use conrod::{widget, Widget, color, Colorable, Borderable};

        let mut result = false;
        let full_transparent = color::rgba(0., 0., 0., 0.);
        let widget = widget::Canvas::new().border(0.).color(full_transparent);
        let canvas_widget_pairs: Vec<_> = Self::values()
            .into_iter()
            .map(|item| (item.canvas_id(ids), widget))
            .collect();

        // Put all the canvas into the main canvas
        widget::Canvas::new()
            .pad(80.)
            .color(full_transparent)
            .flow_down(&canvas_widget_pairs)
            .set(ids.main_canvas, ui);

        // Put all button in its canvas
        for item in Self::values() {
            self.set_button(ui, ids, &mut result, *item);
        }

        result
    }

    pub fn get_choice(window: &mut MyWindow) -> Self {
        let mut result = MainMenu::Play;

        ::view::run_conrod(window, Ids::new, &mut result, Self::gui);
        if window.should_close() {
            result = MainMenu::Quit;
        }
        result
    }
}

widget_ids! {
    pub struct Ids {
        main_canvas,
        
        button_play,
        canvas_play,
        
        button_restart,
        canvas_restart,
        
        button_high,
        canvas_high,

        button_options,
        canvas_options,

        button_about,
        canvas_about,

        button_quit,
        canvas_quit,
    }
}
