use piston_window::{Event, Input, Loop};
use ::{Model, View, Controller, CliOptions};

#[derive(Debug)]
pub struct Game {
    controller: Controller,
    model: Model,
    view: View,
}

impl Game {
    pub fn new(window: &::MyWindow, options: &CliOptions) -> Self {
        Game {
            model: Default::default(),
            view: View::new(window, options),
            controller: Default::default(),
        }
    }

    pub fn restart(&mut self) {
        self.model = Model::default()
    }

    pub fn run(&mut self, window: &mut ::MyWindow) {
        use piston_window::{EventLoop, AdvancedWindow};

        window.set_ups(60);
        window.set_capture_cursor(true);
        while let Some(event) = window.next() {
            match event {
                Event::Input(input_event) => match input_event {
                    Input::Button(b)      => self.controller.input.handle_input(b),
                    Input::Move(motion)   => self.controller.input.cursor_moved(&mut self.model, motion),
                    Input::Text(_)        => (),
                    Input::Resize(_w, _h) => (),
                    Input::Focus(_f)      => (), // pause && open menu
                    Input::Cursor(_)      => (),
                    Input::Close(_args)   => ::std::process::exit(0), //TODO ask to user
                },
                Event::Loop(loop_event)   => match loop_event {
                    Loop::Render(args)    => self.view.render(&self.model, args, window),
                    Loop::AfterRender(_)  => (),
                    Loop::Update(args)    => {
                        self.controller.update(&mut self.model, args);
                        if self.controller.input.has_escape_been_hit() {
                            break;
                        }
                        self.model.update_items(args.dt);
                    },
                    Loop::Idle(_)         => (),
                },
                _                         => (),
            }
        }
        window.set_capture_cursor(false);
    }
}
