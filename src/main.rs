#![feature(non_ascii_idents)]
#![allow(dead_code)] //TODO remove when dev is finished
#![feature(try_from)]
//#![feature(thread_local)]
#![feature(drain_filter)]

extern crate opengl_graphics;
extern crate piston_window;
extern crate sdl2_window;
#[macro_use] extern crate conrod;
extern crate rand;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
#[macro_use] extern crate lazy_static;
extern crate noise;
extern crate structopt;
#[macro_use]
extern crate structopt_derive;


use piston_window::{WindowSettings, PistonWindow};

mod controller;
mod model;
#[macro_use] mod view;

mod game;

mod cli_options;
mod main_menu;

use controller::Controller;
use model::Model;
use view::View;

use game::Game;

type MyWindow = PistonWindow<sdl2_window::Sdl2Window>;

use main_menu::MainMenu;
use cli_options::CliOptions;


pub const FONT_PATH: &str = "assets/FORCED SQUARE.ttf";


fn main() {
    App::new().run()
}


struct App {
    options: CliOptions,
    window: MyWindow,
}

impl Default for App {
    fn default() -> App {
        use structopt::StructOpt;

        let size = [1280, 800];
        let options = CliOptions::from_args();
        let samples = options.samples;
        let version = options.opengl.get();
        let window = WindowSettings::new("my title", size)
            .opengl(version)
            .samples(samples)
            .vsync(true)
            .build()
            .expect("Cannot initialize the window: ");

        App{ options, window }
    }
}

impl App {
    fn new() -> App {
        Self::default()
    }

    fn run(&mut self) {
        let mut game = Game::new(&self.window, &self.options);

        loop {
            game.run(&mut self.window);
            match MainMenu::get_choice(&mut self.window) {
                MainMenu::Play        => continue,
                MainMenu::Restart     => game.restart(),
                MainMenu::HighScores  => (),
                MainMenu::Options     => (),
                MainMenu::About       => (),
                MainMenu::Quit        => break,//self.window.set_should_close(true),
            };
        }
    }
}
