use opengl_graphics::OpenGL;
use ::std::fmt;

#[derive(StructOpt, Debug)]
#[structopt(name = "breake-rs", about = "A brick breaker game.")]
pub struct CliOptions {
    
    #[structopt(short = "s", long = "samples",
        help = "Sets the number of samples for antialiasing", default_value = "16")]
    pub samples: u8,

    #[structopt(short = "o", long = "opengl",
        help = "Sets the OpenGL version to use", default_value = "4.5")]
    pub opengl: OpenGlVersion,
    //#[structopt(short = "o", long = "opengl", help = "Sets the OpenGL version to use", default_value = "4.5")]
    //pub opengl: OpenGL,
}

#[derive(Debug)]
pub struct OpenGlVersion(OpenGL); // delete this when the new version of OpenGL is there

impl ::std::str::FromStr for OpenGlVersion {
    type Err = InvalidVersionError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "2.1" => Ok(OpenGlVersion(OpenGL::V2_1)),
            "3.2" => Ok(OpenGlVersion(OpenGL::V3_2)),
            "4.5" => Ok(OpenGlVersion(OpenGL::V4_5)),
            _     => Err(InvalidVersionError),
        }
    }
}

impl OpenGlVersion {
    pub fn get(&self) -> OpenGL {
        self.0
    }
}


#[derive(Debug)]
pub struct InvalidVersionError;

impl fmt::Display for InvalidVersionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Not a valid OpenGL version")
    }
}

impl ::std::error::Error for InvalidVersionError {
    fn description(&self) -> &str {
        "Invalid OpenGL version"
    }
}

