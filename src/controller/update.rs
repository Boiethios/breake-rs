use ::model::prelude::*;
use ::model::ball::Position;
use ::model::brick::Color;
use piston_window::types::ColorComponent;

impl Grid {
    pub fn update(&mut self, δt: f64) {
        for brick in self.array_mut()
                         .iter_mut()
                         .flat_map(|row| row.iter_mut())
                         .filter_map(|cell| cell.as_mut()) {
            brick.update(δt);
        }
    }
}

impl Balls {
    pub fn update(&mut self, δt: f64, paddle: &Paddle, grid: &mut Grid,
                  boni: &mut Vec<Bonus>, score: &mut Score) {
        for _ in self.as_vec_mut().drain_filter(|ball| {
            if ball.is_moving() {
                ball.collide_with_paddle(paddle);
                ball.collide_with_bricks(grid, boni, score);
                ball.update(δt);
            }
            ball.is_lost()
        }) {
            //println!("Ball is lost!");
        }
    }
}

impl Ball {
    const SPEED_MULT: Scalar = 30.;

    pub fn update(&mut self, δt: f64) {
        if let Position::Moving(ref mut coord) = self.position {
            let direction = self.direction.clone() * Self::SPEED_MULT * δt;
            *coord += direction;
        }
    }
}

impl Bonus {
    const SPEED_MULT: Scalar = 0.1;

    pub fn update(&mut self, δt: f64) {
        self.position.y += Self::SPEED_MULT * δt;
    }
}

impl Brick {
    pub fn update(&mut self, δt: f64) {
        match *self {
            Brick::Unbreakable   => (),
            Brick::Hardened(c)   => *self = Brick::Hardened(c.update(δt)),
            Brick::Reinforced(c) => *self = Brick::Reinforced(c.update(δt)),
            Brick::Normal(c)     => *self = Brick::Normal(c.update(δt)),
            Brick::Brittle(c)    => *self = Brick::Brittle(c.update(δt)),
        }
    }
}

impl Color {
    const DELTA_TIME_MULTIPLIER: ColorComponent = 30.;

    fn update(self, δt: f64) -> Self {
        use piston_window::Colored;
        let delta = δt as ColorComponent * Self::DELTA_TIME_MULTIPLIER;
        
        match self.hue {
            None        => self,
            Some(hue)   => Color {
                               value: Self::MULTI_FIRST_COLOR.hue_deg(hue.into()),
                               hue: Some(hue + delta),
                           }
        }
    }
}
