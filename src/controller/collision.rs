use std::ops::Not;
use ::model::prelude::*;
use ::model::ball::Position as BallPosition;

impl Ball {
    
    /* Paddle collision */
    
    pub fn collide_with_paddle(&mut self, paddle: &Paddle) {
        if self.collide_with(paddle) && self.direction.y > 0. {
            let ratio_paddle_screen = (paddle.size() / 2. + Ball::RADIUS).recip();
            let delta = match self.position {
                BallPosition::Sticked(_)        => unreachable!(),
                BallPosition::Moving(ref coo)   => coo.x - paddle.x()
            } * ratio_paddle_screen;
            let new_direction = Coordinates::new(delta, -(1. - delta.powi(2)).sqrt());
            if new_direction.x.is_nan() || new_direction.y.is_nan() {
                println!("Error.");
            }
            let (old_norme, new_norme) = (self.direction.norme(), new_direction.norme());

            self.direction = new_direction * (old_norme / new_norme);
        }
    }


    /* Brick collision */

    pub fn collide_with_bricks(&mut self, grid: &mut Grid,
                               boni: &mut Vec<Bonus>, score: &mut Score) {
        let (col1, mut row1) = self.where_is_upper_left().unwrap();
        let (col2, mut row2) = self.where_is_lower_right().unwrap();

        if row1 >= grid.array().len() {
            row1 = grid.array().len() - 1;
        }
        if row2 >= grid.array().len() {
            row2 = grid.array().len() - 1;
        }
        let vertical_left = (row1..=row2).fold(0, |acc, row|
            self.must_bounce_on_cell(&mut grid.array_mut()[row][col1]) as i32 + acc);
        let vertical_right = (row1..=row2).fold(0, |acc, row|
            self.must_bounce_on_cell(&mut grid.array_mut()[row][col2]) as i32 + acc);
        let horizontal_up = (col1..=col2).fold(0, |acc, col|
            self.must_bounce_on_cell(&mut grid.array_mut()[row1][col]) as i32 + acc);
        let horizontal_down = (col1..=col2).fold(0, |acc, col|
            self.must_bounce_on_cell(&mut grid.array_mut()[row2][col]) as i32 + acc);

        if (vertical_left > vertical_right && self.direction.x.is_sign_negative())
            || (vertical_right > vertical_left && self.direction.x.is_sign_positive()) {
            self.direction.x *= -1.;
        }
        if (horizontal_up > horizontal_down && self.direction.y.is_sign_negative())
            || (horizontal_down > horizontal_up && self.direction.y.is_sign_positive()) {
            self.direction.y *= -1.;
        }
        self.destroy_bricks(grid, boni, score, row1, row2, col1, col2);
    }

    /// Returns in which cell is the upper left point of the ball
    fn where_is_upper_left(&self) -> Option<(usize, usize)> {
        self.grid_coordinates(true)
    }

    /// Returns in which cell is the lower right point of the ball
    fn where_is_lower_right(&self) -> Option<(usize, usize)> {
        self.grid_coordinates(false)
    }

    fn grid_coordinates(&self, upper_left: bool) -> Option<(usize, usize)> {
        fn get_coord(coord: &Coordinates, upper_left: bool) -> (usize, usize) {
            let (center_x, center_y) = (coord.x, coord.y);
            let adjust = if upper_left { -Ball::RADIUS } else { Ball::RADIUS };
            let (x, y) = (center_x + adjust, center_y + adjust);
            ((x / CELL_WIDTH) as usize, (y / CELL_HEIGHT) as usize)
        }
        match self.position {
            BallPosition::Sticked(_)        => None,
            BallPosition::Moving(ref coord) => Some(get_coord(&coord, upper_left)),
        }
    }

    fn must_bounce_on_cell(&self, cell: &GridCell) -> bool {
        match *cell {
            None        => false,
            Some(brick) => match brick {
                Brick::Unbreakable |
                Brick::Hardened(_)   => true,
                Brick::Reinforced(_) |
                Brick::Normal(_)     => self.is_steel().not(),
                Brick::Brittle(_)    => false,
            }
        }
    }

    fn destroy_bricks(&self, grid: &mut Grid, boni: &mut Vec<Bonus>, score: &mut Score,
                      row1: usize, row2: usize, col1: usize, col2: usize) {
        use rand::{Rng, thread_rng};
        let array = grid.array_mut();

        for cell in array[row1..=row2].iter_mut()
                     .flat_map(|line| line[col1..=col2].iter_mut())
                     .filter(|cell| cell.is_some()) {
            let brick = cell.unwrap();
            match brick {
                Brick::Unbreakable                      => (),
                Brick::Hardened(_) if self.is_steel()   => *cell = None,
                Brick::Hardened(_)                      => (),
                Brick::Reinforced(_) if self.is_steel() => *cell = None,
                Brick::Reinforced(c)                    => *cell = Some(Brick::Normal(c)),
                Brick::Normal(_)                        => *cell = None,
                Brick::Brittle(_)                       => *cell = None,
            };
            if cell.is_none() {
                score.brick_broken(brick); // TODO increment score when Reinforced -> Normal
                if thread_rng().gen_range(0, 100) < 40 {
                    if let BallPosition::Moving(coo) = self.position {
                        boni.push(Bonus::new(coo)); // TODO coordinates of the brick
                    }
                }
            }
        }
    }
}

impl Bonus {
    pub fn is_being_caught(&self, paddle: &mut Paddle, balls: &mut Balls,
                           score: &mut Score) -> bool {
        use ::model::bonus::Kind;
        let collision = self.collide_with(paddle);

        if collision {
            match self.kind {
                Kind::PaddleGrowth => paddle.increase_size(),
                Kind::PaddleShrink => paddle.decrease_size(),
                Kind::BallSpeedUp => (),
                Kind::BallSpeedDown => (),
                Kind::BallHarden => balls.harden(),
                Kind::BallMultiplier(n) => balls.multiply(n),
            };
            score.bonus_caught(self.kind);
        }
        collision
    }
}
