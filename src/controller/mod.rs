mod input;
mod collision;
mod update;

use piston_window::UpdateArgs;
use ::model::Model;

pub use self::input::InputController;

#[derive(Debug, Default)]
pub struct Controller {
    pub input: InputController,
}

impl Controller {
    pub fn new() -> Controller {
        Controller::default()
    }

    pub fn update(&mut self, model: &mut Model, args: UpdateArgs) {
        if self.input.state.left {
            model.paddle.update(-args.dt);
        }
        if self.input.state.right {
            model.paddle.update(args.dt);
        }
        if self.input.state.fire {
            model.fire();
            self.input.state.fire = false;
        }
        if self.input.state.h {
            model.balls.harden();
            self.input.state.h = false;
        }
    }
}