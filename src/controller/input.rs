use piston_window::{Button, ButtonArgs, ButtonState, Key, MouseButton, Motion};
use piston_window::types::Scalar;
use ::model::Model;
use std::ops::Not;

const MOUSE_RATIO: Scalar = 200.;

#[derive(Debug, Default)]
pub struct InputController {
    pub state: ActionState,
    escape_has_been_hit: bool,
    //magnet: bool,
}

#[derive(Debug, Default)]
pub struct ActionState {
    pub left: bool,
    pub right: bool,
    pub fire: bool,
    pub h: bool,
}

impl InputController {
//    pub fn new() -> InputController {
//       InputController::default()
//    }

    pub fn has_escape_been_hit(&mut self) -> bool {
        ::std::mem::replace(&mut self.escape_has_been_hit, false)
    }

    pub fn handle_input(&mut self, args: ButtonArgs) {
        let is_pressed = match args.state {
            ButtonState::Press   => true,
            ButtonState::Release => false,
        };
        
        self.handle_control(args.button, is_pressed)
    }

    fn handle_control(&mut self, b: Button, is_pressed: bool) {
        match b {
            Button::Keyboard(Key::Escape)
                => self.escape_has_been_hit = is_pressed.not(),
            Button::Keyboard(Key::H)
                => self.state.h = is_pressed,
            Button::Keyboard(Key::Left)
                => self.state.left = is_pressed,
            Button::Keyboard(Key::Right)
                => self.state.right = is_pressed,
            Button::Keyboard(Key::Space) | Button::Mouse(MouseButton::Left)
                => self.state.fire = is_pressed,
            _   => (),
        }
    }


    pub fn cursor_moved(&mut self, model: &mut Model, motion: Motion) {
        if let Motion::MouseRelative(x, _) = motion {
            model.paddle.update(x as Scalar / MOUSE_RATIO);
        }
    }
}