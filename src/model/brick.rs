use std::fmt;
use piston_window::types::{Color as PistonColor, ColorComponent};

#[derive(Debug, Clone, Copy)]
pub enum Brick {
    Unbreakable,
    /// Only steel ball can break it
    Hardened(Color),
    /// Must be hit twice
    Reinforced(Color),
    /// Must be hit once
    Normal(Color),
    /// Ball do not bonce on it
    Brittle(Color),
}

impl Brick {
    pub fn is_breakable(&self, is_ball_hardened: bool) -> bool {
        match (is_ball_hardened, *self) {
            (_, Brick::Unbreakable) | (false, Brick::Hardened(_)) => false,
            _ => true,
        }
    }
}

/* Color */

#[derive(Debug, Clone, Copy)]
pub struct Color {
    pub value: PistonColor,
    pub hue: Option<Hue>,
}

impl Color {
    pub const MULTI_FIRST_COLOR: PistonColor = [0.9, 0.9, 0.5, 1.];

    fn new(color: PistonColor) -> Self {
        Color {
            value: color,
            hue: None,
        }
    }

    fn multicolor(mut self) -> Self {
        self.hue = Some(Hue::default());

        self
    }
}

#[derive(Debug)]
pub struct InvalidCharError(char);

impl fmt::Display for InvalidCharError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "`{}` is not a valid color", self.0)
    }
}

impl ::std::error::Error for InvalidCharError {
    fn description(&self) -> &str {
        "Invalid color character"
    }
}

impl ::std::convert::TryFrom<char> for Color {
    type Error = InvalidCharError;

    fn try_from(c: char) -> Result<Self, Self::Error> {
        match c {
            'B' => Ok(Color::new([0.5, 0.5, 0.9, 1.])),
            'C' => Ok(Color::new([0.5, 0.9, 0.9, 1.])),
            'G' => Ok(Color::new([0.5, 0.9, 0.5, 1.])),
            'M' => Ok(Color::new([0.9, 0.5, 0.9, 1.])),
            'R' => Ok(Color::new([0.9, 0.5, 0.5, 1.])),
            'Y' => Ok(Color::new([0.9, 0.9, 0.5, 1.])),
            '*' => Ok(Color::new(Self::MULTI_FIRST_COLOR).multicolor()),
            c   => Err(InvalidCharError(c)),
        }
    }
}

/* Hue */

#[derive(Debug, Clone, Copy, Default)]
pub struct Hue(ColorComponent);

impl From<ColorComponent> for Hue {
    fn from(n: ColorComponent) -> Self {
        Hue(n)
    }
}

impl From<Hue> for ColorComponent {
    fn from(hue: Hue) -> ColorComponent {
        hue.0
    }
}

impl ::std::ops::Add<ColorComponent> for Hue {
    type Output = Hue;

    fn add(self, rhs: ColorComponent) -> Self {
        let mut new_value = self.0 + rhs;

        if new_value > 360. {
            new_value -= 360.;
        }
        Hue(new_value)
    }
}
