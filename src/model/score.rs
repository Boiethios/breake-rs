use ::model::brick::Brick;
use ::model::bonus::Kind;

#[derive(Debug, Default)]
pub struct Score(usize);

impl Score {
    pub fn get(&self) -> usize {
        self.0
    }

    pub fn level_won(&mut self) {
        self.0 += 100
    }

    pub fn brick_broken(&mut self, brick: Brick) {
        self.0 += match brick {
            Brick::Hardened(_)   => 10,
            Brick::Reinforced(_) => 5,
            Brick::Normal(_)     => 3,
            Brick::Brittle(_)    => 1,
            _                    => return,
        }
    }

    pub fn bonus_caught(&mut self, kind_of_bonus: Kind) {
        self.0 += match kind_of_bonus {
            Kind::PaddleGrowth      => 5,
            Kind::PaddleShrink      => 15,
            Kind::BallSpeedDown     => 5,
            Kind::BallSpeedUp       => 15,
            Kind::BallHarden        => 10,
            Kind::BallMultiplier(n) => n as usize,
        }
    }
}
