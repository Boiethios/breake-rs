use ::model::prelude::*;

#[derive(Debug)]
pub struct Bonus {
    pub position: Coordinates,
    pub kind: Kind,
}

#[derive(Debug, Copy, Clone)]
pub enum Kind {
    PaddleGrowth,
    PaddleShrink,

    BallSpeedUp,
    BallSpeedDown,

    BallHarden,

    BallMultiplier(u8),
}

impl Collidable for Bonus {
    fn center(&self) -> Coordinates {
        self.position
    }
    fn dimension(&self) -> Coordinates {
        Coordinates::new(Self::SEMI_SIDE, Self::SEMI_SIDE)
    }
}

impl Bonus {
    const SEMI_SIDE: Scalar = 0.03;

    fn generate_random_type() -> Kind {
        use rand::{Rng, thread_rng};

        match thread_rng().gen_range(0, 100) {
            0...19      => Kind::PaddleGrowth,
            20...39    => Kind::PaddleShrink,
            101 => Kind::BallSpeedUp,
            102 => Kind::BallSpeedDown,
            40...49 => Kind::BallHarden,
            50...100 => Kind::BallMultiplier(3),
            _           => unreachable!()
        }
    }

    pub fn new(position: Coordinates) -> Bonus {
        Bonus {
            position,
            kind: Self::generate_random_type(),
        }
    }

    pub fn is_lost(&self) -> bool {
        self.position.y > 1. - Self::SEMI_SIDE
    }
}
