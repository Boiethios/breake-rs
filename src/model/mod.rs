/*
 * Put here the objets model drawn on the screen, eg
 * brick, paddle, ball, etc.
*/

pub mod ball;
pub mod bonus;
pub mod brick;
pub mod collidable;
pub mod coordinates;
pub mod grid;
pub mod paddle;
pub mod score;

pub mod prelude {
    pub use piston_window::types::Scalar;
    
    pub const N_OF_COLS: usize = 24; // <->
    pub const N_OF_ROWS: usize = 20; // ^|v
    pub const CELL_WIDTH: Scalar = 1. / N_OF_COLS as Scalar;
    pub const CELL_HEIGHT: Scalar = 1. / N_OF_ROWS as Scalar;

    pub fn identity<T>(t: T) -> T {t}

    pub use super::Model;
    pub use super::ball::{Ball, Balls};
    pub use super::bonus::Bonus;
    pub use super::brick::{self, Brick};
    pub use super::collidable::Collidable;
    pub use super::coordinates::Coordinates;
    pub use super::grid::{Grid, Cell as GridCell};
    pub use super::paddle::Paddle;
    pub use super::score::Score;
}

use self::prelude::*;

#[derive(Debug, Default)]
pub struct Model {
    pub grid: Grid,
    pub paddle: Paddle,
    pub balls: Balls,
    pub boni: Vec<Bonus>,
    pub score: Score,
    //shots
}

impl Model {
    pub fn new() -> Model {
        Default::default()
    }

    pub fn update_items(&mut self, δt: f64) {
        let (paddle, grid, boni, balls, score) =
            (&mut self.paddle, &mut self.grid, &mut self.boni, &mut self.balls, &mut self.score);

        // TODO: move to balls::Update
        balls.update(δt, paddle, grid, boni, score);

        boni.drain_filter(|bonus| {
            let result = bonus.is_being_caught(paddle, balls, score) || bonus.is_lost();
            bonus.update(δt);
            
            result
        });

        grid.update(δt);
        if grid.is_level_finished(balls.as_slice()) {
            *balls = Balls::default();
            *boni = Vec::new();
            score.level_won();
            grid.next_level();
        }
    }

    pub fn fire(&mut self) {
        let ref paddle = &self.paddle;

        // If no ball was sticked, fire a bullet
        if self.balls.as_slice_mut().iter_mut().fold(true, |acc, ball| match ball.is_moving() {
                true    => acc,
                false   => { ball.launch(paddle); false },
            }) {
            /*TODO attempt to fire*/
        }
    }
}