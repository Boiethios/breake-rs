use ::model::prelude::*;

const DEFAULT_SIZE: Scalar = 0.1;
const MAX_SIZE: Scalar = DEFAULT_SIZE * 2.;
const MIN_SIZE: Scalar = DEFAULT_SIZE / 3.;

#[derive(Debug)]
pub struct Paddle {
    x: Scalar,
    size: Scalar,
}

impl Default for Paddle {
    fn default() -> Paddle {
        Paddle{
            x: 0.5,
            size: DEFAULT_SIZE,
        }
    }
}

impl Collidable for Paddle {
    fn center(&self) -> Coordinates {
        Coordinates{x: self.x, y: Self::Y}
    }

    fn dimension(&self) -> Coordinates {
        Coordinates::new(self.size / 2., Self::SEMI_HEIGHT)
    }
}

impl Paddle {
    pub const SEMI_HEIGHT: Scalar = 0.015;
    pub const Y: Scalar = 0.975;

    pub fn x(&self) -> Scalar {
        self.x
    }

    
    pub fn size(&self) -> Scalar {
        self.size
    }

    pub fn increase_size(&mut self) {
        if self.size < MAX_SIZE {
            self.size *= 1.2;
        }
    }

    pub fn decrease_size(&mut self) {
        if self.size > MIN_SIZE {
            self.size *= 0.8;
        }
    }


    pub fn update(&mut self, delta: Scalar) {
        let min = self.size() / 2.;
        let max = 1. - min;

        self.x += delta;
        if self.x < min {
            self.x = min;
        } else if self.x > max {
            self.x = max;
        }
    }
}