use ::model::prelude::*;
use ::std::ops;

#[derive(Debug, Clone, Copy)]
pub struct Coordinates {
    pub x: Scalar,
    pub y: Scalar,
}

impl Into<(Scalar, Scalar)> for Coordinates {
    fn into(self) -> (Scalar, Scalar) {
        (self.x, self.y)
    }
}

impl ops::Mul<Scalar> for Coordinates {
    type Output = Coordinates;

    fn mul(self, rhs: Scalar) -> Coordinates {
        Coordinates::new(self.x * rhs, self.y * rhs)
    }
}

impl ops::Mul<Coordinates> for Scalar {
    type Output = Coordinates;

    fn mul(self, rhs: Coordinates) -> Coordinates {
        rhs * self
    }
}

impl ops::Sub<Coordinates> for Coordinates {
    type Output = Coordinates;

    fn sub(self, rhs: Coordinates) -> Coordinates {
        Coordinates::new(self.x - rhs.x, self.y - rhs.y)
    }
}

impl ops::Add<Coordinates> for Coordinates {
    type Output = Coordinates;

    fn add(self, rhs: Coordinates) -> Coordinates {
        Coordinates::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl ops::AddAssign for Coordinates {
    fn add_assign(&mut self, rhs: Coordinates) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl Coordinates {
    pub fn new(x: Scalar, y: Scalar) -> Self {
        Coordinates {x: x, y: y}
    }

    pub fn norme(&self) -> Scalar {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }

    pub fn rotate(self, angle: Scalar) -> Self {
        let x = self.x * angle.cos() - self.y * angle.sin();
        let y = self.x * angle.sin() + self.y * angle.cos();

        Self::new(x, y)
    }
}
