use ::model::prelude::*;

#[derive(Debug, Copy, Clone)]
pub struct Ball {
    pub position: Position,
    pub direction: Coordinates,
        steel: bool,
}

impl Default for Ball {
    fn default() -> Ball {
        Ball {
            position: Default::default(),
            direction: Coordinates::new(0., -0.01),
            steel: false,
        }
    }
}

impl Collidable for Ball {
    fn center(&self) -> Coordinates {
        match self.position {
            Position::Sticked(_) => unreachable!("Do not use Collidable with sticked balls"),
            Position::Moving(ref coord) => coord.clone(),
        }
    }

    fn dimension(&self) -> Coordinates {
        // maybe we should adapt the dimension to make the ball circle
        Coordinates::new(Self::RADIUS, Self::RADIUS)
    }
}

impl Ball {
    pub const RADIUS: Scalar = 0.013;
    pub const MAX_NUMBER: usize = 1000;

    pub fn new() -> Ball {
        Default::default()
    }

    fn from_ball(other: &Ball) -> Ball {
        use rand::{Rng, thread_rng};

        let angle = thread_rng().gen_range(0., ::std::f64::consts::PI);

        Ball {
            direction: other.direction.rotate(angle),
            ..*other
        }
    }

    pub fn is_moving(&self) -> bool {
        match self.position {
            Position::Moving(_)    => true,
            Position::Sticked(_)   => false,
        }
    }

    pub fn position(&self, paddle: &Paddle) -> Coordinates {
        match self.position {
            Position::Sticked(offset)   => {
                let radius_on_x = paddle.size() / 2.;
                let (x, y) = paddle.center().into();
                let x = x + (radius_on_x * offset);
                let y = y - Paddle::SEMI_HEIGHT - Self::RADIUS;
                Coordinates::new(x, y)
            },
            Position::Moving(ref coord) => coord.clone(),
        }
    }

    pub fn is_lost(&self) -> bool {
        match self.position {
            Position::Sticked(_)    => false,
            Position::Moving(ref p) => p.y > 1.,
        }
    }


    pub fn is_steel(&self) -> bool {
        self.steel
    }

    fn harden(&mut self) {
        self.steel = true
    }


    pub(super) fn launch(&mut self, paddle: &Paddle) {
        self.position = Position::Moving(self.position(paddle));
    }
}


/*
 * Balls
 */

#[derive(Debug)]
pub struct Balls(Vec<Ball>);

impl Default for Balls {
    fn default() -> Self {
        Balls(vec![Ball::default()])
    }
}

impl Balls {
    pub fn as_slice(&self) -> &[Ball] {
        &self.0
    }

    pub fn as_slice_mut(&mut self) -> &mut [Ball] {
        &mut self.0
    }

    pub fn as_vec_mut(&mut self) -> &mut Vec<Ball> {
        &mut self.0
    }

    pub fn harden(&mut self) {
        self.0.iter_mut().for_each(Ball::harden)
    }

    pub fn multiply(&mut self, n: u8) {
        let create_new_multiplier = |ball| BallMultiplier::new(ball, n);
        let new_balls = self.0.clone().into_iter().flat_map(create_new_multiplier);

        self.0.extend(new_balls);
        self.0.truncate(Ball::MAX_NUMBER);
        println!("Now, there are {} balls", self.0.len())
    }
}


/*
 * BallMultiplier
 */

struct BallMultiplier{
    template: Ball,
    remaining: u8,
}

impl BallMultiplier {
    fn new(template: Ball, remaining: u8) -> Self {
        BallMultiplier{ template, remaining }
    }
}

impl Iterator for BallMultiplier {
    type Item = Ball;

    fn next(&mut self) -> Option<Self::Item> {
        self.remaining -= 1;

        match self.remaining {
            0 => None,
            _ => Some(Ball::from_ball(&self.template)),
        }
    }
}


/*
 * Position
 */

#[derive(Debug, Copy, Clone)]
pub enum Position {
    Sticked(Scalar),
    Moving(Coordinates),
}

impl Default for Position {
    fn default() -> Position {
        Position::Sticked(0.)
    }
}
