use ::model::prelude::*;
use std::error::Error;
use std::ops::Not;

pub type Cell = Option<Brick>;
type BricksArray = [[Cell; N_OF_COLS]; N_OF_ROWS];


#[derive(Debug, Deserialize)]
struct LevelJson {
    title: String,
    grid: Vec<[String; N_OF_COLS - 2]>,
}


#[derive(Debug)]
pub struct Grid {
    array: BricksArray,
    level: u32,
    title: String,
}

impl Default for Grid {
    fn default() -> Grid {
        let mut grid = Grid{
            array: BricksArray::default(),
            level: 0u32,
            title: String::new(),
        };

        grid.next_level();
        grid
    }
}

impl Grid {
    pub fn array(&self) -> &BricksArray {
        &self.array
    }

    pub fn array_mut(&mut self) -> &mut BricksArray {
        &mut self.array
    }

    pub fn title(&self) -> &str {
        &self.title
    }


    pub fn is_level_finished(&mut self, balls: &[Ball]) -> bool {
        let at_least_one_hardened_ball = balls.iter().any(|ball| ball.is_steel());

        self.array.iter()
                  .skip(1)
                  .flat_map(|row| row.iter().skip(1).take(N_OF_COLS - 2))
                  .filter_map(|cell| cell.as_ref())
                  .any(|brick| brick.is_breakable(at_least_one_hardened_ball))
                  .not()
    }


    fn reset_array(&mut self) {
        self.array = BricksArray::default();
        self.array[0] = [Some(Brick::Unbreakable); N_OF_COLS];
        for row in self.array.iter_mut().skip(1) {
            row[0] = Some(Brick::Unbreakable);
            row[N_OF_COLS - 1] = Some(Brick::Unbreakable);
        }
    }

    pub fn next_level(&mut self) {
        self.level += 1;
        self.array = *EMPTY_ARRAY;

        if let Err(e) = self.read_level() {
            panic!("Cannot read level: {}", e);
        }
    }

    fn read_level(&mut self) -> Result<(), Box<Error>> {
        use std::convert::TryFrom;
        use std::fs::File;
        use std::io::Read;

        // open file
        let file_name = format!("levels/{:02}.lvl", self.level);
        let mut content = String::new();
        File::open(file_name)?.read_to_string(&mut content)?;
        let level_json: LevelJson = ::serde_json::from_str(&content)?;

        for (line, row) in level_json.grid.into_iter()
                                          .zip(self.array.iter_mut().skip(1)) {
            for (s, cell) in line.iter()
                                 .zip(row.iter_mut().skip(1).take(N_OF_COLS - 2)) {
                let mut chars = s.trim().chars();
                let (kind, color) = (chars.next(), chars.next()
                    .unwrap_or_else(| | { /*println!("No color specified: default is Blue");*/ 'B' }));
                *cell = match kind {
                    Some('u') => Some(Brick::Unbreakable),
                    Some('h') => Some(Brick::Hardened(brick::Color::try_from(color)?)),
                    Some('r') => Some(Brick::Reinforced(brick::Color::try_from(color)?)),
                    Some('n') => Some(Brick::Normal(brick::Color::try_from(color)?)),
                    Some('b') => Some(Brick::Brittle(brick::Color::try_from(color)?)),
                    _         => None,
                };
            }
        }
        self.title = level_json.title;
        Ok(())
    }
}

lazy_static! {
    static ref EMPTY_ARRAY: BricksArray = {
        let mut array = BricksArray::default();
        array[0] = [Some(Brick::Unbreakable); N_OF_COLS];
        for row in array.iter_mut().skip(1) {
            row[0] = Some(Brick::Unbreakable);
            row[N_OF_COLS - 1] = Some(Brick::Unbreakable);
        };
        array
    };
}
