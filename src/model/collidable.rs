pub use model::prelude::*;

pub trait Collidable {
    fn center(&self) -> Coordinates;
    fn dimension(&self) -> Coordinates;

    fn collide_with(&self, other: &Collidable) -> bool {
        let self_center = self.center();
        let self_dim = self.dimension();
        let (self_1, self_2) = (self_center - self_dim, self_center + self_dim);

        let other_center = other.center();
        let other_dim = other.dimension();
        let (other_1, other_2) = (other_center - other_dim, other_center + other_dim);

        other_1.x < self_2.x &&
        other_2.x > self_1.x &&
        other_1.y < self_2.y &&
        other_2.y > self_1.y
    }
}
