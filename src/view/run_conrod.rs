use conrod::widget::id::Generator;
use piston_window::{self, G2d, G2dTexture, TextureSettings, Event, Input, Button, Key, ButtonState};
use piston_window::texture::UpdateTexture;
use ::MyWindow;
use ::conrod;


pub fn run_conrod<T, ID>(
        window: &mut MyWindow,
        ids_factory: fn(Generator) -> ID,
        mut t: &mut T,
        gui: fn(&mut T, &mut conrod::UiCell, &ID) -> bool) {

    use piston_window::Window;

    let ::piston_window::Size {width, height} = window.size();
    let mut ui = conrod::UiBuilder::new([width as f64, height as f64])
        //.theme()
        .build();
    let ids = ids_factory(ui.widget_id_generator());
    ui.fonts.insert_from_file(::FONT_PATH).unwrap();
    let mut text_vertex_data = Vec::new();
    let (mut glyph_cache, mut text_texture_cache) = {
        const SCALE_TOLERANCE: f32 = 0.1;
        const POSITION_TOLERANCE: f32 = 0.1;
        let cache = conrod::text::GlyphCache::new(
            width, height, SCALE_TOLERANCE, POSITION_TOLERANCE);
        let buffer_len = width as usize * height as usize;
        let init = vec![128; buffer_len];
        let settings = TextureSettings::new();
        let factory = &mut window.factory;
        let texture = G2dTexture::from_memory_alpha(factory, &init, width, height, &settings)
            .unwrap();
        (cache, texture)
    };

    let image_map = conrod::image::Map::new();

    let mut finished = false;

    while let Some(event) = window.next() {
        use piston_window::UpdateEvent;
        let size = window.size();

        if let Event::Input(Input::Button(args)) = event {
            if let (Button::Keyboard(Key::Escape), ButtonState::Release) = (args.button, args.state) {
                break;
            }
            match (args.button, args.state) {
                (Button::Keyboard(Key::Escape), ButtonState::Release) => break,
                _ => (),
            }
        }

        let (win_w, win_h) = (size.width as conrod::Scalar, size.height as conrod::Scalar);
        if let Some(e) = conrod::backend::piston::event::convert(event.clone(), win_w, win_h) {
            ui.handle_event(e);
        }
        event.update(|_| {
            let mut ui = ui.set_widgets();
            finished = gui(&mut t, &mut ui, &ids);
        });

        window.draw_2d(&event, |context, graphics| {
            if let Some(primitives) = ui.draw_if_changed() {
                let cache_queued_glyphs = |graphics: &mut G2d,
                                            cache: &mut G2dTexture,
                                            rect: conrod::text::rt::Rect<u32>,
                                            data: &[u8]|
                {
                    let offset = [rect.min.x, rect.min.y];
                    let size = [rect.width(), rect.height()];
                    let format = piston_window::texture::Format::Rgba8;
                    let encoder = &mut graphics.encoder;
                    text_vertex_data.clear();
                    text_vertex_data.extend(data.iter().flat_map(|&b| vec![255, 255, 255, b]));
                    UpdateTexture::update(cache, encoder, format,
                        &text_vertex_data[..], offset, size)
                        .expect("failed to update texture")
                };
                fn texture_from_image<T>(img: &T) -> &T { img }
                conrod::backend::piston::draw::primitives(
                    primitives,
                    context,
                    graphics,
                    &mut text_texture_cache,
                    &mut glyph_cache,
                    &image_map,
                    cache_queued_glyphs,
                    texture_from_image
                );
            }
        });
        if finished {
            break;
        }
    }
}
