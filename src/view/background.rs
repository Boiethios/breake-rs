use piston_window::{Polygon, Context, color};
use piston_window::types::{Scalar, ColorComponent};
use opengl_graphics::GlGraphics;

use noise::{NoiseModule, Seedable, Perlin};
use rand::{Rng, thread_rng, ThreadRng};

const DIMENSION: usize = 20;

#[derive(Debug, Default, Copy, Clone)]
struct Vertex {
    pub x: Scalar,
    pub y: Scalar,
    pub colors: (ColorComponent, ColorComponent),
    pub way: bool,
}

type VertexArray = [[Vertex; DIMENSION]; DIMENSION];

#[derive(Debug, Copy, Clone)]
pub struct Background {
    vertices: VertexArray,
}

impl Default for Background {
    fn default() -> Self {
        fn generate_height_noise(rand: &mut ThreadRng, vertices: &mut VertexArray) {
            let seed = rand.gen();
            let noise = Perlin::new().set_seed(seed);
            let zoom = rand.gen_range(2., 5.);
            let redistribution = rand.gen_range(0.3, 0.9);
            let brightness = rand.gen_range(4., 6.);
            let dim = DIMENSION as Scalar / 2.;

            for (y, row) in vertices.iter_mut().enumerate() {
                for (x, cell) in row.iter_mut().enumerate() {
                    let (x, y) = (x as Scalar, y as Scalar);
                    let (nx, ny) = ((x - dim) / zoom, (y - dim) / zoom);
                    let gradient = (noise.get([nx, ny]) as ColorComponent + 1.) / brightness;
                    let (x, y) = (x / (DIMENSION as Scalar - 1.), y / (DIMENSION as Scalar - 1.));
                    let colors = (gradient.powf(redistribution), 0.);

                    *cell = Vertex { x, y, colors, way: rand.gen() };
                }
            }
            Background::log_min_max(vertices);
        }

        fn shuffle_vertices(rand: &mut ThreadRng, vertices: &mut VertexArray) {
            let shuffle_range = rand.gen_range(0.1, 0.5);
            let shuffle_max = shuffle_range / DIMENSION as Scalar;
            let shuffle_min = -shuffle_max;

            for row in vertices.iter_mut().skip(1).take(DIMENSION - 2) {
                for vertex in row.iter_mut().skip(1).take(DIMENSION - 2) {
                    vertex.x += rand.gen_range(shuffle_min, shuffle_max);
                    vertex.y += rand.gen_range(shuffle_min, shuffle_max);
                }
            }
        }

        /* Color of a triangle is average of colors of its vertices */
        fn get_vertices_with_finalized_colors(old_vertices: VertexArray) -> VertexArray {
            let mut vertices = old_vertices.clone();
            {
                let iterate_on_vertices = vertices.iter_mut()
                                                .take(DIMENSION - 1)
                                                .flat_map(|row| row.iter_mut().take(DIMENSION - 1));

                for (vertices_pair_init, vertex_end) in
                    old_vertices.windows(2)
                                .flat_map(|rows| rows[0].windows(2).zip(rows[1].windows(2)))
                                .zip(iterate_on_vertices) {
                    let (points1, points2) = vertices_pair_init;
                    let (p0_0, p0_1) = (&points1[0], &points1[1]);
                    let (p1_0, p1_1) = (&points2[0], &points2[1]);

                    let color0 = 1. - (p0_0.colors.0 + p0_1.colors.0 + p1_0.colors.0) / 3.;
                    let color1 = 1. - (p1_1.colors.0 + p0_1.colors.0 + p1_0.colors.0) / 3.;

                    vertex_end.colors = (color0, color1);
                }
            }
            vertices
        }

        let mut rand = thread_rng();
        let mut vertices_init = VertexArray::default();

        generate_height_noise(&mut rand, &mut vertices_init);
        shuffle_vertices(&mut rand, &mut vertices_init);
        let vertices = get_vertices_with_finalized_colors(vertices_init);
        
        Background{ vertices }
    }
}

impl Background {
    pub fn draw(&self, context: &Context, gl: &mut GlGraphics) {
        for vertices in self.vertices.windows(2)
                                     .flat_map(|rows| rows[0].windows(2).zip(rows[1].windows(2))) {
            let (points1, points2) = vertices;
            let (p0_0, p0_1) = (&points1[0], &points1[1]);
            let (p1_0, p1_1) = (&points2[0], &points2[1]);
            let color0 = p0_0.colors.0;
            let color1 = p0_0.colors.1;

            let third_vertex = if p0_0.way { p1_0 } else { p1_1 };
            let triangle = [[p0_0.x, p0_0.y], [p0_1.x, p0_1.y], [third_vertex.x, third_vertex.y]];
            Polygon::new(color::grey(color0))
                .draw(&triangle, &context.draw_state, context.view.clone(), gl);

            let third_vertex = if p0_0.way { p0_1 } else { p0_0 };
            let triangle = [[p1_1.x, p1_1.y], [p1_0.x, p1_0.y], [third_vertex.x, third_vertex.y]];
            Polygon::new(color::grey(color1))
                .draw(&triangle, &context.draw_state, context.view.clone(), gl);
        }
    }

    #[cfg(not(debug_assertions))]
    fn log_min_max(_: &mut VertexArray) {}

    #[cfg(debug_assertions)]
    fn log_min_max(vertices: &mut VertexArray) {
        fn cmp(a: &f32, b: &f32) -> ::std::cmp::Ordering {
            a.partial_cmp(b).expect(&format!("Error when comparing {:?} and {:?}", a, b))
        }
        println!("[Background noise] min height: {}", vertices.iter()
            .flat_map(|row| row).map(|v| v.colors.0).min_by(cmp).expect("no min value"));
        println!("[Background noise] max height: {}", vertices.iter()
            .flat_map(|row| row).map(|v| v.colors.0).max_by(cmp).expect("no max value"));
    }
}
