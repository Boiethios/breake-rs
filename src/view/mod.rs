mod background;
pub mod run_conrod;

pub use self::run_conrod::run_conrod;

use std::fmt;
use piston_window::{clear, rectangle, ellipse, Context, Ellipse, Rectangle, Polygon, Colored,
    Viewport, RenderArgs, Glyphs, TextureSettings};
use piston_window::types::{Color, Scalar, ColorComponent, SourceRectangle};
use opengl_graphics::GlGraphics;
use ::CliOptions;
use std::cell::RefCell;

use model::prelude::*;

use self::background::Background;

/*
 * View
 */

pub struct View {
    background: Background,
    glyphs: Glyphs,
    gl: RefCell<GlGraphics>,
}

impl fmt::Debug for View {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "View {{ background: {:?} }}", self.background)
    }
}

impl View {
    const RATIO_VIEWPORTS: i32 = 15;

    pub fn new(window: &::MyWindow, options: &CliOptions) -> View {
        let background = Background::default();
        let factory = window.factory.clone();
        let glyphs = Glyphs::new(::FONT_PATH, factory, TextureSettings::new())
            .expect("Cannot create glyphs");
        let gl = RefCell::new(GlGraphics::new(options.opengl.get()));
        
        View { background, glyphs, gl }
    }

    pub fn render(&mut self, model: &Model, args: RenderArgs, window: &mut ::MyWindow) {
        {
            let mut gl = self.gl.borrow_mut();
            
            gl.draw(Self::game_viewport(args.viewport()),
                |context, gl| self.draw_game(model, context, gl));
            gl.draw(Self::infos_viewport(args.viewport()),
                |context, gl| self.draw_infos(model, context, gl));
        }
        self.draw_text(model, window, args);
    }

    fn draw_str(&mut self, window: &mut ::MyWindow, args: RenderArgs,
                s: &str, coo: (Scalar, Scalar)) {
        use piston_window::{Event, Loop, text, Transformed};
        let event = &Event::Loop(Loop::Render(args));

        window.draw_2d(event, |c, g| {
            let transform = c.transform.trans(coo.0, coo.1);

            text::Text::new_color([0.0, 1.0, 0.0, 1.0], 32)
                .draw(s, &mut self.glyphs, &c.draw_state, transform, g);
        });
    }

    fn draw_text(&mut self, model: &Model, window: &mut ::MyWindow, args: RenderArgs) {
        let coo = (10., 10. + 32.);
        self.draw_str(window, args, model.grid.title(), coo);

        let coo = (1130., 10. + 32.);
        let score = format!("{:06}", model.score.get());
        self.draw_str(window, args, &score, coo);
    }

    fn draw_infos(&self, _: &Model, context: Context, gl: &mut GlGraphics) {
        use piston_window::Transformed;

        let viewport = context.viewport.expect("Fatal: context does not have viewport");
        let (w, h) = (viewport.rect[2] as Scalar, viewport.rect[3] as Scalar);
        let matrix = context.transform.scale(w, h);

        Rectangle::new([1., 0., 1.0, 1.0]).draw([0., 0., 1.0, 1.0], &context.draw_state, matrix, gl)
    }

    fn draw_game(&self, model: &Model, mut context: Context, gl: &mut GlGraphics) {
        use piston_window::Transformed;

        let viewport = context.viewport.expect("Fatal: context does not have viewport");
        let (w, h) = (viewport.rect[2] as Scalar, viewport.rect[3] as Scalar);
        let ratio = h / w;
        context.transform = context.transform.scale(w, h);
        let context = context.store_view();

        clear([0., 0., 0., 0.], gl);
        self.background.draw(&context, gl);
        model.grid.draw(model, &context, gl);
        model.paddle.draw(model, &context, gl);
        for ball in model.balls.as_slice() {
            ball.draw(model, ratio, &context, gl);
        }
        for bonus in model.boni.iter() {
            bonus.draw(&context, gl);
        }
    }

    fn infos_viewport(orig: Viewport) -> Viewport {
        let x = orig.rect[3] / Self::RATIO_VIEWPORTS;

        Viewport {
            rect: [0, orig.rect[3] - x, orig.rect[2], x],
            ..orig
        }
    }

    fn game_viewport(orig: Viewport) -> Viewport {
        let x = orig.rect[3] / Self::RATIO_VIEWPORTS;

        Viewport {
            rect: [0, 0, orig.rect[2], orig.rect[3] - x],
            ..orig
        }
    }
}


/*
 * Grid
 */

impl Grid {
    pub fn draw(&self, _: &Model, context: &Context, gl: &mut GlGraphics) {
        let ref array = self.array();
        let (w, h) = (CELL_WIDTH * 0.92, CELL_HEIGHT * 0.92);

        for (col, &line) in array.iter().enumerate() {
            for (row, &cell) in line.iter().enumerate() {
                if let Some(brick) = cell {
                    let (x, y) = (CELL_WIDTH * row as Scalar, CELL_HEIGHT * col as Scalar);
                    brick.draw(context, gl, [x, y, w, h]);
                }
            }
        }
    }
}


/*
 * Ball
 */

impl Ball {
    const NORMAL_COLOR: Color = [0.8, 0.1, 0.1, 1.0];
    const HARDENED_COLOR: Color = [0.3, 0.35, 0.55, 1.0];

    pub fn draw(&self, model: &Model, ratio: Scalar, context: &Context, gl: &mut GlGraphics) {
        let (x, y) = self.position(&model.paddle).into();

        self.circle().border(ellipse::Border{ color: self.color().shade(0.1), radius: 0.001 }).draw(
            rectangle::centered([x, y, Ball::RADIUS * ratio, Ball::RADIUS]),
            &context.draw_state,
            context.view.clone(),
            gl,
        );
    }

    fn circle(&self) -> Ellipse {
        Ellipse::new(self.color())
    }

    fn color(&self) -> Color {
        match self.is_steel() {
            false => Self::NORMAL_COLOR,
            true  => Self::HARDENED_COLOR,
        }
    }
}


/*
 * Paddle
 */

impl Paddle {
    const COLOR: Color = [0.3, 0.1, 0.9, 1.0];

    pub fn draw(&self, _: &Model, context: &Context, gl: &mut GlGraphics) {
        let (x, y) = self.center().into();
        let (w, h) = self.dimension().into();

        self.rect().border(rectangle::Border{ color: Self::COLOR.shade(0.2), radius: 0.002 }).draw(
            rectangle::centered([x, y, w, h]),
            &context.draw_state,
            context.view.clone(),
            gl,
        )
    }

    pub fn rect(&self) -> Rectangle {
        Rectangle::new_round(Self::COLOR, 0.01)
    }
}


/*
 * Brick
 */

impl Brick {
    pub fn draw(&self, context: &Context, gl: &mut GlGraphics, dim: SourceRectangle) {
        match *self {
            Brick::Unbreakable   => Self::draw_unbreakable(context, gl, dim),
            Brick::Hardened(c)   => Self::draw_hardened(context, gl, dim, c),
            Brick::Reinforced(c) => Self::draw_reinforced(context, gl, dim, c),
            Brick::Normal(c)     => Self::draw_normal(context, gl, dim, c),
            Brick::Brittle(c)    => Self::draw_brittle(context, gl, dim, c),
        }
    }

    fn draw_unbreakable(context: &Context, gl: &mut GlGraphics, dim: SourceRectangle) {
        use piston_window::Rectangled;

        Rectangle::new([0.4, 0.4, 0.4, 1.0])
            .border(rectangle::Border{ color: [0.1, 0.1, 0.1, 1.0], radius: 0.002 })
            .draw(dim.margin(0.001), &context.draw_state, context.view.clone(), gl);
    }

    fn draw_hardened(context: &Context, gl: &mut GlGraphics,
            dim: SourceRectangle, c: brick::Color) {
        let (x1, y1) = (dim[0], dim[1]);
        let (x2, y2) = (x1 + dim[2], y1 + dim[3]);
        let (xc, yc) = (x1 + dim[2] / 2., y1 + dim[3] / 2.);
        
        Rectangle::new(c.normal())
            .border(rectangle::Border{ color: c.dark(), radius: 0.001 })
            .draw(dim, &context.draw_state, context.view.clone(), gl);
        Polygon::new(c.dark())
            .draw(&[[x1, y1], [x2, y1], [xc, yc]], &context.draw_state, context.view.clone(), gl);
        Polygon::new(c.dark())
            .draw(&[[x2, y2], [x1, y2], [xc, yc]], &context.draw_state, context.view.clone(), gl);
    }

    fn draw_reinforced(context: &Context, gl: &mut GlGraphics,
            dim: SourceRectangle, c: brick::Color) {
        use piston_window::Rectangled;

        let margin = 0.004;

        Rectangle::new(c.normal())
            .border(rectangle::Border{ color: c.dark(), radius: 0.001 })
            .draw(dim, &context.draw_state, context.view.clone(), gl);
        Rectangle::new(c.normal().shade(0.1))
            .border(rectangle::Border{ color: c.dark(), radius: 0.001 })
            .draw(dim.margin(margin), &context.draw_state, context.view.clone(), gl);
    }

    fn draw_normal(context: &Context, gl: &mut GlGraphics,
            dim: SourceRectangle, c: brick::Color) {
        Rectangle::new(c.normal())
            .border(rectangle::Border{ color: c.dark(), radius: 0.001 })
            .draw(dim, &context.draw_state, context.view.clone(), gl);
    }

    fn draw_brittle(context: &Context, gl: &mut GlGraphics,
            dim: SourceRectangle, c: brick::Color) {
        Rectangle::new(c.transparent())
            .border(rectangle::Border{ color: c.dark(), radius: 0.001 })
            .draw(dim, &context.draw_state, context.view.clone(), gl);
    }
}

impl brick::Color {
    fn normal(self) -> [ColorComponent; 4] {
        self.value
    }

    fn dark(self) -> [ColorComponent; 4] {
        self.normal().shade(0.4)
    }

    fn transparent(self) -> [ColorComponent; 4] {
        let mut result = self.normal();

        result[3] -= 0.7;
        result
    }
}

impl Bonus {
    pub fn draw(&self, context: &Context, gl: &mut GlGraphics) {
        use ::model::bonus::Kind;

        let color = match self.kind {
            Kind::PaddleGrowth => [0., 1., 0., 1.],
            Kind::PaddleShrink => [1., 0., 0., 1.],
            Kind::BallSpeedUp => [0., 1., 0.5, 1.],
            Kind::BallSpeedDown => [1., 0., 0.5, 1.],
            Kind::BallHarden => [0., 0., 1., 1.],
            Kind::BallMultiplier(_) => [0.4, 0.4, 0.4, 1.],
        };
        let (x, y) = self.position.into();
        let (w, h) = self.dimension().into();
        Rectangle::new(color)
            //.border(rectangle)
            .draw([x, y, w, h], &context.draw_state, context.view.clone(), gl);
    }

}
